//app/index.js
const MongoClient = require('mongodb').MongoClient;
//const assert = require('assert');
const url = 'mongodb://localhost:27017';

MongoClient.connect(url, { useNewUrlParser: true, useUnifedTopology: true }, function (err, client) {
    console.log("connected to db");
    const db = client.db("mydb");

    // insertDocuments(db, function () {
    //     client.close();
    // });
    updateDocument(db, function () {
        client.close();
    });
});

const insertDocuments = function (db, callback) {
    // Get the documents collection
    const collection = db.collection('documents');
    // Insert some documents
    collection.insertMany([
        { Name: "Ruslan" , SerName: "Tyo",LastName: "Serggevich" },  { Name: "Ruslan" , SerName: "Tyo",LastName: "Serggevich" },  { Name: "Ruslan" , SerName: "Tyo",LastName: "Serggevich" }
    ], function (err, result) {
        console.log("Inserted 3 documents into the collection");
        callback(result);
    });
}


const updateDocument = function(db, callback) {
    // Get the documents collection
    const collection = db.collection('documents');
    // Update document where a is 2, set b equal to 1
    collection.updateOne({ Name : "Ruslan" }
      , { $set: { Name : "Bruce" } }, function(err, result) {
      console.log("Updated the document with the field a equal to 2");
      callback(result);
    });
  }

