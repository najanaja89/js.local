
// let test = require("test");
// console.log(test);
// function start(){
//     console.log("test");
// }
// console.log(module);
var http = require("http");
var human = require("human");
var hw17  = require("hw17/main.js");
var myrouter = require("router");

//custom events
var EE = require("events").EventEmitter;
var serverEE = new EE();

serverEE.on("connected", function(e){
    console.log(e);
});
serverEE.on("chatstart", function(e){
    console.log(e);
})

//установка supervisor: npm i -g supervisor
//запуск supervisor ./index.js

function connect() {
    
    var server = new http.Server();
    server.listen(1337, "localhost");
    //console.log("server started");
    server.on("request", function (req, res) {
        //console.log(req.url)
        serverEE.emit("connected", "server connected");
        if(req.url=="/about")
        {
           res.setHeader('Content-Type', 'text/html');
           res.write("about ");
        }
        // if(req.url=="/chat")
        // {
        //    res.setHeader('Content-Type', 'text/html');
        //    serverEE.emit("chatstart", "chat starting, please wait");
        // }
        myrouter.router(req.url, res);
        res.end("my text2");
    })
}

function humanUse(){
    console.log(human.name);
    human.walk();
    human.eat();
    human.talk();
}

function hw17use(){
    hw17.delete_();
    hw17.read();
    hw17.write();
}

if (module.parent){
    module.exports.connect = connect;
    module.exports.human = human;
    module.exports.hw17 = hw17;
    //module.exports.myrouter = myrouter;
}
else {
    connect();
    serverEE.emit("myevent", "myeventLog");
    humanUse();
    hw17use();
}